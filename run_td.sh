echo 'Do you want the debug mode ? (y/n)';
read -n1 ans;

mode=2;
if [[ $ans =~ ^[Yy]$ ]]; then
    mode=1;
fi;

echo -e "\n";

echo -e "\e[1m\e[36m--- FIFO: ramSize=3 ---\e[0m";
./main 1 3 $mode tests/1.txt;

echo -e "\e[1m\e[36m--- FIFO: ramSize=4 ---\e[0m";
./main 1 4 $mode tests/1.txt;

echo -e "\e[1m\e[36m--- FIFO: ramSize=4 ---\e[0m";
./main 1 4 $mode tests/2.txt;

echo -e "\e[1m\e[36m--- NRU: ramSize=4 ---\e[0m";
./main 2 4 $mode tests/2.txt;

echo -e "\e[1m\e[36m--- CLOCK: ramSize=4 ---\e[0m";
./main 4 4 $mode tests/2.txt;

echo -e "\e[1m\e[36m--- OPTIMAL: ramSize=4 ---\e[0m";
./main 5 4 $mode tests/2.txt;
