#include "../include/ordo.h"

/* Struct creation */

Params* newParams(){
    Params* p = mylloc(sizeof(Params));

    p->algo = 5;
    p->ramSize = 10;
    p->debug = 0;

    return p;
}

Process* newProcess(Params* params){
    Process* p = mylloc(sizeof(Process));
    int i;

    p->t = 0;
    p->loaded = newList();
    p->params = params;
    p->defaults = 0;

    p->pages = mylloc(params->nbPage * sizeof(Page));
    for(i = 0; i < params->nbPage; i++)
         p->pages[i] = (Page){0, 0, 0, 0};

    return p;
}

/* private: algorithms implementation */

int _runFIFO(Process* p){
    int currAcces;
    int debug = p->params->debug;

    printf("FIFO\n");

    /* for each access */
    for(p->t = 0; p->t < p->params->totalAccess; p->t++){
        currAcces = p->params->acces[p->t];
            
        if(debug)
            printf("\e[1mpage %d: \e[0m", currAcces);

        /* access not valid */
        if(!p->pages[currAcces].b_valide){
            if(debug)
                printf("\e[32m(loading) \e[0m");
            push(currAcces, &(p->loaded));
            p->pages[currAcces].b_valide = 1;

            /* RAM full */
            if(length(p->loaded) > p->params->ramSize){
                if(debug)
                    printf("\e[31m(RAM full, switching pages) \e[0m");
                p->pages[shift(&(p->loaded))].b_valide = 0;
                p->defaults++;
            }
            if(debug)
                printf("\n");
        /* acess valid */
        }else if(debug){
            printf("ok\n");
        }
        
        /* show ram */
        if(debug){
            printf("  \e[2m");
            printList(p->loaded);
            printf("\e[0m");
        }
    }
    printf("Default(s): %d\n", p->defaults);
    return 0;
}

int _runNRU(Process* p){
    int currAcces;
    int debug = p->params->debug;

    printf("NRU\n");

    /* for each access */
    for(p->t = 0; p->t < p->params->totalAccess; p->t++){
        currAcces = p->params->acces[p->t];
            
        if(debug)
            printf("\e[1mpage %d: \e[0m", currAcces);

        /* access not valid */
        if(!p->pages[currAcces].b_valide){
            if(debug)
                printf("\e[32m(loading) \e[0m");
            unshift(currAcces, &(p->loaded));
            p->pages[currAcces].b_valide = 1;

            /* RAM full */
            if(length(p->loaded) > p->params->ramSize){
                if(debug)
                    printf("\e[31m(RAM full, switching pages) \e[0m");
                p->pages[pop(&(p->loaded))].b_valide = 0;
                p->defaults++;
            }
            if(debug)
                printf("\n");
        /* acess valid */
        }else{
            if(debug)
                printf("ok\n");
            delByValue(currAcces, &(p->loaded));
            unshift(currAcces, &(p->loaded));
        }
        
        /* show ram */
        if(debug){
            printf("  \e[2m");
            printList(p->loaded);
            printf("\e[0m");
        }
    }
    printf("Default(s): %d\n", p->defaults);
    return 0;
}

int _runCLOCK(Process* p){
    int currAcces;
    int debug = p->params->debug;
    List* searchList;
    el testedPageIndex;
    int found;

    printf("CLOCK\n");

    /* for each access */
    for(p->t = 0; p->t < p->params->totalAccess; p->t++){
        currAcces = p->params->acces[p->t];
            
        if(debug)
            printf("\e[1mpage %d: \e[0m", currAcces);

        /* access not valid */
        if(!p->pages[currAcces].b_valide){
            if(debug)
                printf("\e[32m(loading) \e[0m");
            p->pages[currAcces].b_valide = 1;
            p->pages[currAcces].b_acces = 1;

            /* RAM will be full */
            if(length(p->loaded) + 1 > p->params->ramSize){
                if(debug)
                    printf("\e[31m(RAM full, switching pages) \e[0m");
                /* victim research */
                searchList = newList();
                found = 0;
                while(!isEmpty(p->loaded)){
                    testedPageIndex = shift(&(p->loaded));
                    if(!p->pages[testedPageIndex].b_acces){
                        /* victim found */
                        push(currAcces, &(p->loaded));
                        found = 1;
                        p->pages[testedPageIndex].b_valide = 0;
                        break;
                    }
                    p->pages[testedPageIndex].b_acces = 0;
                    push(testedPageIndex, &searchList);
                }
                /* p->loaded restoration */
                while(!isEmpty(searchList)){
                    unshift(pop(&searchList), &(p->loaded));
                }
                if(!found){
                    p->pages[shift(&(p->loaded))].b_valide = 0;
                    push(currAcces, &(p->loaded));
                }

                p->defaults++;
            }else{
                push(currAcces, &(p->loaded));
            }
            if(debug)
                printf("\n");
        /* acess valid */
        }else{
            if(debug)
                printf("ok\n");
        }
        
        /* show ram */
        if(debug){
            printf("  \e[2m");
            printList(p->loaded);
            printf("\e[0m");
        }
    }
    printf("Default(s): %d\n", p->defaults);
    return 0;
}

int _runOPTIMAL(Process* p){
    int currAcces;
    int debug = p->params->debug;

    int numSuspect;
    int* suspects;
    int* stats;
    int i;
    int victim;

    printf("OPTIMAL\n");

    /* for each access */
    for(p->t = 0; p->t < p->params->totalAccess; p->t++){
        currAcces = p->params->acces[p->t];
            
        if(debug)
            printf("\e[1mpage %d: \e[0m", currAcces);

        /* access not valid */
        if(!p->pages[currAcces].b_valide){
            if(debug)
                printf("\e[32m(loading) \e[0m");
            p->pages[currAcces].b_valide = 1;
            p->pages[currAcces].b_acces = 1;

            push(currAcces, &(p->loaded));

            /* RAM full */
            if(length(p->loaded) > p->params->ramSize){
                if(debug)
                    printf("\e[31m(RAM full, switching pages) \e[0m");

                /* compute stats for all suspects */
                numSuspect = 0;
                suspects = mylloc((p->params->ramSize + 1) * sizeof(int));
                stats = mylloc((p->params->ramSize + 1) * sizeof(int));

                for(i = 0; i < p->params->ramSize + 1; i++){
                    stats[i] = 0;
                }

                while(!isEmpty(p->loaded)){ /* for each suspect */
                    suspects[numSuspect] = pop(&(p->loaded));
                    if(suspects[numSuspect] == currAcces){
                        /* make currAcces a god */
                        stats[numSuspect] = INT_MIN;
                    }else{
                        for(i = p->t; i < p->params->totalAccess; i++){ /* compute suspect' stats */
                            if(p->params->acces[i] == suspects[numSuspect])
                                stats[numSuspect] = i;
                        }
                    }
                    numSuspect++;
                }

                /* search the max in stats */
                victim = -1;
                for(i = 0; i < p->params->ramSize + 1; i++){
                    if(victim == -1){
                        victim = i;
                    }else if(stats[i] == 0){
                        victim = i;
                        break;
                    }else if(stats[victim] < stats[i]){
                        victim = i;
                    }
                }
                victim = suspects[victim];
                free(stats);

                /* victim found, restoring p->loaded without it */
                for(i = 0; i < p->params->ramSize + 1; i++){
                    if(suspects[i] != victim)
                        push(suspects[i], &(p->loaded));
                }
                free(suspects);

                p->pages[victim].b_valide = 0;

                p->defaults++;
            }
            if(debug)
                printf("\n");
        /* acess valid */
        }else{
            if(debug)
                printf("ok\n");
        }
        
        /* show ram */
        if(debug){
            printf("  \e[2m");
            printList(p->loaded);
            printf("\e[0m");
        }
    }
    printf("Default(s): %d\n", p->defaults);
    return 0;
}

int _runLRU(Process* p){
    int currAcces;
    int debug = p->params->debug;

    int freq[p->params->nbPage];
    int i, min;

    printf("LRU\n");

    /* freq init */
    for(i = 0; i < p->params->nbPage; i++){
        freq[i] = 0;
    }

    /* for each access */
    for(p->t = 0; p->t < p->params->totalAccess; p->t++){
        currAcces = p->params->acces[p->t];

        freq[currAcces]++;
            
        if(debug)
            printf("\e[1mpage %d: \e[0m", currAcces);

        /* access not valid */
        if(!p->pages[currAcces].b_valide){
            if(debug)
                printf("\e[32m(loading) \e[0m");

            /* RAM will be full */
            if(length(p->loaded) + 1 > p->params->ramSize){
                if(debug)
                    printf("\e[31m(RAM full, switching pages) \e[0m");
                
                /* victim research */
                min = -1;
                for(i = 0; i < p->params->nbPage; i++){
                    if(isIn(i, &(p->loaded))){
                        if(min == -1 || freq[min] > freq[i]){
                            min = i;
                        }
                    }
                }

                delByValue(min, &(p->loaded));
                p->pages[min].b_valide = 0;

                p->defaults++;
            }

            /* currAcces insertion */
            unshift(currAcces, &(p->loaded));
            p->pages[currAcces].b_valide = 1;

            if(debug)
                printf("\n");
        /* acess valid */
        }else{
            if(debug)
                printf("ok\n");
            delByValue(currAcces, &(p->loaded));
            unshift(currAcces, &(p->loaded));
        }
        
        /* show ram */
        if(debug){
            printf("  \e[2m");
            printList(p->loaded);
            printf("\e[0m");
        }
    }
    printf("Default(s): %d\n", p->defaults);
    return 0;
}

/* main run */

int processRun(Process* p){
    switch(p->params->algo){
        case 1:
            return _runFIFO(p);
        case 2:
            return _runNRU(p);
        case 3:
            return _runLRU(p);
        case 4:
            return _runCLOCK(p);
        default:
            return _runOPTIMAL(p); 
    }
}

/* load_args and utilities */

void _read_file(char* path, Params* p){
    FILE* f;
    char buffer[255];
    int tmp;
    int i = 0;

    if(strcmp(path, "") == 0)
        f = stdin;
    else{
        f = fopen(path, "r");
        if(f == NULL){
            throw_err("Specified file do not exists or the access permission is denied");      
        }
    }
    
    while(fscanf(f, "%s", buffer) != EOF){
        if((tmp = atoi(buffer)) != 0 || buffer[0] == '0'){
            if(i == 0)
                p->totalAccess = tmp;
            else if(i == 1)
                p->nbPage = tmp;
            else{
                if(i == 2)
                    p->acces = mylloc(p->totalAccess * sizeof(int));
                p->acces[i-2] = tmp;
            }
        }else
            throw_err("Error in file format");
        i++;
    }

    if(i - 2 != p->totalAccess)
        throw_err("Error in file format");
}

void load_args(int argc, char* argv[], Params* p){
    int i, pIndex = 1;

    for(i = 1; i < argc; i++){
        if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0){
            usage();
        }else{
            switch(pIndex){
                case 1:
                    p->algo = atoi(argv[i]);
                    if(p->algo == 0 || p->algo < 1 || p->algo > 5){
                        usage();
                    }
                    break;
                case 2:
                    p->ramSize = atoi(argv[i]);
                    if(p->ramSize == 0 || p->ramSize <= 0){
                        usage();
                    }
                    break;
                case 3:
                    p->debug = atoi(argv[i]);
                    if(p->debug == 0 || p->debug < 1 || p->debug > 2){
                        usage();
                    }
                    if(p->debug == 2)
                        p->debug = 0;
                    break;
                case 4:
                    _read_file(argv[i], p);
                    break;
            }
            pIndex++;
        }
    }

    if(pIndex < 4){
        _read_file("", p);
    }
}




