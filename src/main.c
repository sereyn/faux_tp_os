#include <stdio.h>
#include <stdlib.h>

#include "../include/ordo.h"

int main(int argc, char* argv[]){
    Params* params;
    Process* p1;
    
    /* loading paramaters and process initialization */
    params = newParams();
    load_args(argc, argv, params);
    p1 = newProcess(params);

    /* run process */
    processRun(p1);

    exit(EXIT_SUCCESS);
}

