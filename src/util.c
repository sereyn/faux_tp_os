#include "../include/util.h"

void usage(){
    printf("This program require 4 parameters:\n");
    printf("    1. [optionnal](int) Algorithm: 1=FIFO 2=NRU 3=LRU 4=horloge 5=optimal(default)\n");
    printf("    2. [optionnal](int) RAM size: number of blocks allocated (10 by default)\n");
    printf("    3. [optionnal](int) Display mode: 1=DEBUG or 2=CLASSIC(default)\n");
    printf("    4. [optionnal](file) Memory access formatted file (stdin by default)\n");
    printf("\nOPTIONS:\n");
    printf("    -h --help : print this message\n");
    exit(EXIT_SUCCESS);
}

void* mylloc(size_t s){
    void* p = malloc(s);
    if(p == NULL){
        throw_err("mylloc impossible");
    }
    return p;
}

void throw_err(char* msg){
    fprintf(stderr, "\n[x] Error: ");
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}


