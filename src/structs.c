#include "../include/structs.h"

/* USEFULL */

void mem_err(char* msg){
    printf("Memory error: ");
    printf("%s", msg);
    printf("\n");
    exit(EXIT_FAILURE);
}

/* LIST IMPLEMENTATION */

List* newList(){
    return NULL;
}

int isEmpty(List* l){
    return l == NULL;
}

void push(el e, List** l){
    if(isEmpty(*l)){
        *l = (List*)malloc(sizeof(List));
        if(*l == NULL)
            mem_err("Stacking impossible");
        (*l)->value = e;
        (*l)->next = NULL;
    }else{
        push(e, &((*l)->next));
    }
}

el pop(List** l){
    el v;
    if(isEmpty(*l)){
        fprintf(stderr, "List already empty!\n");
        exit(EXIT_FAILURE);
    }
    if(isEmpty((*l)->next)){
        v = (*l)->value;
        free(*l);
        *l = NULL;
        return v;
    }else
        return pop(&((*l)->next));
}

void unshift(el e, List** l){
    List* n;
    if(isEmpty(*l)){
        *l = malloc(sizeof(List));
        if(*l == NULL)
            mem_err("Unshift impossible");
        (*l)->value = e;
        (*l)->next = NULL;
    }else{
        n = malloc(sizeof(List));
        if(n == NULL)
            mem_err("Unshift impossible");
        n->value = e;
        n->next = *l;
        *l = n;
    }
}

el shift(List** l){
    el v;
    List* future_first;
    if(isEmpty(*l)){
        fprintf(stderr, "List already empty!\n");
        exit(EXIT_FAILURE);
    }
    v = (*l)->value;
    future_first = (*l)->next;
    free(*l);
    (*l) = future_first;
    return v;
}

void printListAux(List* l){
    if(l == NULL)
        printf(">\n");
    else{
        printf("%d ", l->value);
        printListAux(l->next);
    }
}

void printList(List* l){
    printf("< ");
    printListAux(l);
}

int length(List* l){
    int c = 0;
    while(l != NULL){
        l = l->next;
        c++;
    }
    return c;
}

void del(List** l){
    if(isEmpty(*l))
        return;
    del(&((*l)->next));
    free(*l);
    *l = NULL;
}

int delByValue(el e, List** l){
    List* tmp;

    if(isEmpty(*l))
        return 0;

    if((*l)->value == e){
        tmp = *l;
        *l = (*l)->next;
        free(tmp);
        return 1;
    }
    return delByValue(e, &((*l)->next));
}

int isIn(el e, List** l){
    int in = 0;
    el curr;
    List* save = newList();

    while(!isEmpty(*l)){
        curr = pop(l);
        push(curr, &save);
        if(curr == e){
            in = 1;
            break;
        }
    }

    while(!isEmpty(save)){
        push(pop(&save), l);
    }

    return in;
}
