#ifndef UTIL
#define UTIL

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct Params;

void usage();

void* mylloc(size_t s);

void throw_err(char* msg);

#endif /* UTIL */

