#ifndef STRUCTS
#define STRUCTS

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Type depends on your needs */
typedef int el;

/* List struct */
typedef struct cell{
    struct cell* next;
    el value;
} List;

/* LIST FUNCTIONS */

List* newList();

void push(el e, List** l);

el pop(List** l);

void unshift(el e, List** l);

el shift(List** l);

int isEmpty(List* l);

void printList(List* l);

int length(List* l);

void del(List** l);

int delByValue(el e, List** l);

int isIn(el e, List** l);

#endif
