#ifndef ORDO
#define ORDO

#include <limits.h>
#include "util.h"
#include "structs.h"

typedef struct Params{
    int algo;
    int ramSize;
    int debug;

    int totalAccess;
    int nbPage;
    int* acces;
} Params;

typedef struct Page{
    int b_valide;
    int b_acces;
    int b_modif;
    int dernier_acces;
} Page;

typedef struct Process{
    int t; /* execution time */
    List* loaded; /* index of loaded pages */
    Page* pages;
    Params* params;
    int defaults; /* nb of defaults */
} Process;

Params* newParams();

Process* newProcess(Params* params);

int processRun(Process* p);

void load_args(int argc, char* argv[], struct Params* p);

#endif /* ORDO */
